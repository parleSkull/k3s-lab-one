# Variable declarations
variable "namespace" {
  description = "Organization name"
  type        = string
}

variable "environment" {
  description = "Environment for this project e.g. 'pov', 'prod', 'staging', 'dev', 'UAT'"
  type        = string
}

variable "stage" {
  description = "Stage in lifecycle of this project e.g. 'prod', 'staging', 'dev'"
  type        = string
}

variable "name" {
  description = "Solution name or title of this project e.g. 'myapp'"
  type        = string
}

variable "resource_tags" {
  description = "Additional tags to set for all resources"
  type        = map(string)
  default     = {
    project     = "smproject",
    owner = "stevem"
  }

  validation {
    condition     = length(var.resource_tags["project"]) <= 16 && length(regexall("/[^a-zA-Z0-9-]/", var.resource_tags["project"])) == 0
    error_message = "The project tag must be no more than 16 characters, and only contain letters, numbers, and hyphens."
  }

  validation {
    condition     = length(var.resource_tags["owner"]) <= 8 && length(regexall("/[^a-zA-Z0-9-]/", var.resource_tags["owner"])) == 0
    error_message = "The owner tag must be no more than 8 characters, and only contain letters, numbers, and hyphens."
  }
}

variable "aws_region" {
  description = "AWS region"
  type        = string
}

variable "aws_auth_profile" {
  description = "AWS authentication profile"
  type        = string
}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet_count" {
  description = "Number of public subnets."
  type        = number
  default     = 2
}

variable "private_subnet_count" {
  description = "Number of private subnets."
  type        = number
  default     = 2
}

variable "public_subnet_cidr_blocks" {
  description = "Available cidr blocks for public subnets."
  type        = list(string)
  default     = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
    "10.0.4.0/24",
    "10.0.5.0/24",
    "10.0.6.0/24",
    "10.0.7.0/24",
    "10.0.8.0/24",
  ]
}

variable "private_subnet_cidr_blocks" {
  description = "Available cidr blocks for private subnets."
  type        = list(string)
  default     = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24",
    "10.0.104.0/24",
    "10.0.105.0/24",
    "10.0.106.0/24",
    "10.0.107.0/24",
    "10.0.108.0/24",
  ]
}

variable "remote_admin_cidr_blocks" {
  description = "CIDR blocks for administrative connections."
  type        = list(string)
  default = ["0.0.0.0/0"]
}

variable "ec2_instance_type" {
  description = "AWS EC2 instance type."
  type        = string
}

variable "ssh_key_path" {
  description = "SSH Public Key path"
  type        = string
}

variable "key_pair_name" {
  description = "AWS key pair name"
  type        = string
}
