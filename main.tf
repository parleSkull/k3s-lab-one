# Terraform config
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = var.aws_region
  profile = "terraform_user"
}

# Locals
locals {
  tags = module.global_labels.tags
  vpc_id = module.vpc.vpc_id
  kp_id = module.vpc.key_pair_id
  bastion_subnet = module.vpc.public_subnets[0]
  bastion_cidr_block = module.vpc.public_subnets_cidr_blocks[0]
  worker_pub_subnets = slice(module.vpc.public_subnets, 0, var.public_subnet_count)
  worker_priv_subnets = slice(module.vpc.private_subnets, 0, var.private_subnet_count)
  worker_pub_sn_cidr_blocks = slice(module.vpc.public_subnets_cidr_blocks, 0, var.public_subnet_count)
  worker_priv_sn_cidr_blocks = slice(module.vpc.private_subnets_cidr_blocks, 0, var.private_subnet_count)
}

# Modules #

# Global labels for some resources
module "global_labels" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=master"
  namespace  = var.namespace
  environment = var.env
  stage      = var.stage
  name       = var.name
  delimiter  = "-"

  tags = var.resource_tags
}

# VPC
module "vpc" {
  source  = "./modules/vpc"

  cidr = var.vpc_cidr_block

  private_subnets = slice(var.private_subnet_cidr_blocks, 0, var.private_subnet_count)
  public_subnets  = slice(var.public_subnet_cidr_blocks, 0, var.public_subnet_count)

  ssh_key_path = var.ssh_key_path
  key_pair_name = var.key_pair_name

  tags = local.tags
}

# Bastion
module "bastion" {
  source = "./modules/bastion"

  vpc_id                 = local.vpc_id
  subnet_id = local.bastion_subnet
  instance_type          = var.ec2_instance_type
  key_pair_id = local.kp_id
  remote_admin_cidr_blocks        = var.remote_admin_cidr_blocks

  tags                   = local.tags
}

# Worker
module "worker" {
  source = "./modules/worker"

  vpc_id                 = local.vpc_id
  pub_subnets      = local.worker_pub_subnets
  priv_subnets      = local.worker_priv_subnets
  pub_sn_cidr_blocks      = local.worker_pub_sn_cidr_blocks
  priv_sn_cidr_blocks      = local.worker_priv_sn_cidr_blocks
  instance_type          = var.ec2_instance_type
  key_pair_id = local.kp_id

  tags                   = local.tags
}

# CP
module "control-plane" {
  source = "./modules/control-plane"

  vpc_id                 = local.vpc_id
  subnet_id = local.bastion_subnet
  instance_type          = var.ec2_instance_type
  key_pair_id = local.kp_id
  remote_admin_cidr_blocks        = var.remote_admin_cidr_blocks

  tags                   = local.tags
}
