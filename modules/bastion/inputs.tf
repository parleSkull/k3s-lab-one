# Input variables
variable "tags" {
  description = "Tags for resources"
  type        = map(any)
  default     = {}
}

variable "vpc_id" {
  description = "ID of VPC to associate with"
  type        = string
}

variable "remote_admin_cidr_blocks" {
  description = "CIDR blocks for SSH connection sources"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "egress_cidr_blocks" {
  description = "CIDR blocks for egress traffic"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "instance_type" {
  description = "Type of EC2 instance to use"
  type        = string
}

variable "key_pair_id" {
  description = "Key pair Id for the bastion instance"
  type        = string
}

variable "subnet_id" {
  description = "Subnet ID for bastion"
  type        = string
}
