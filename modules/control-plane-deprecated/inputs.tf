# Input variables
variable "tags" {
  description = "Tags for resources"
  type        = map(any)
  default     = {}
}

variable "vpc_id" {
  description = "ID of VPC to associate with"
  type        = string
}

variable "ssh_cidr_blocks" {
  description = "CIDR blocks for SSH connection sources"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "icmp_cidr_blocks" {
  description = "CIDR blocks for ICMP connection sources"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "internet_cidr_blocks" {
  description = "CIDR blocks for internet connection destinations"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "instance_type" {
  description = "Type of EC2 instance to use"
  type        = string
}

variable "instance_key_pair_name" {
  description = "Key pair Id for the cp instance"
  type        = string
}

variable "subnet_cidr_block" {
  description = "CIDR block for cp subnet"
  type        = string
}

variable "subnet_avail_zone" {
  description = "Availability zone for cp subnet"
  type        = string
}

variable "bastion_sn_cidr_block" {
  description = "CIDR block for bastion subnet"
  type        = string
}

variable "worker_sn_cidr_block" {
  description = "CIDR block for worker subnet"
  type        = string
}
