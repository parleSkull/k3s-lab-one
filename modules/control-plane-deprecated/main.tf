# Control plane module definition

# Control plane Subnet
resource "aws_subnet" "cp_sn" {
  vpc_id            = var.vpc_id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.subnet_avail_zone

  tags = var.tags
}

# Control plane Security group
resource "aws_security_group" "cp_sg" {
  name        = "cp-sg-allow-ssh-http"
  description = "Allow SSH and HTTP from bastion"

  vpc_id = var.vpc_id
  tags   = var.tags

  ingress {
    description = "SSH from bastion"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.bastion_sn_cidr_block]
  }
  ingress {
    description = "HTTP from bastion"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.bastion_sn_cidr_block]
  }
  ingress {
    description = "HTTPS from bastion"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.bastion_sn_cidr_block]
  }
  egress {
    description = "All traffic to workers"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.worker_sn_cidr_block]
  }
}

# Control plane EC2 instance data
data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
# Control plane EC2 instance
resource "aws_instance" "cp" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type

  key_name = var.instance_key_pair_name

  subnet_id                   = aws_subnet.cp_sn.id
  vpc_security_group_ids      = [aws_security_group.cp_sg.id]
  associate_public_ip_address = false
  source_dest_check           = false

  tags = var.tags
}
