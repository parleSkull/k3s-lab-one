# Bastion module definition

# Bastion Security group
resource "aws_security_group" "bastion" {
  name        = "bastion-allow-ssh-http"
  description = "Allow incoming SSH"

  vpc_id = var.vpc_id
  tags   = var.tags

  ingress {
    description = "SSH from specific remote admin addresses"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.remote_admin_cidr_blocks
  }
  egress {
    description = "Generic internet access for generic operations"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.egress_cidr_blocks
  }
}

# Bastion EC2 instance data
data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
# Bastion EC2 instance
resource "aws_instance" "bastion" {
  ami                         = data.aws_ami.amazon_linux.id
  instance_type               = var.instance_type
  key_name                    = var.key_pair_id
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = [aws_security_group.bastion.id]
  associate_public_ip_address = true
  source_dest_check           = false

  tags = var.tags
}
