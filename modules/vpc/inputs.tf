# Input variables
variable "tags" {
  description = "Tags for resources"
  type        = map(any)
  default     = {}
}

variable "cidr" {
  description = "The CIDR block for the VPC."
  type        = string
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
  default     = []
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
  default     = []
}

variable "ssh_key_path" {
  description = "SSH Public Key path"
  type        = string
  default = ""
}

variable "key_pair_name" {
  description = "Default AWS key pair name"
  type        = string
  default = ""
}
