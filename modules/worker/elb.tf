# Worker App LB
resource "aws_lb" "worker" {
  name               = "worker"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.worker_nodes.id,aws_security_group.worker_alb.id]
  subnets            = [var.pub_subnets[0], var.pub_subnets[1]]

  tags = var.tags
}
# Worker LB Target group
resource "aws_lb_target_group" "worker_http" {
  name     = "worker-http"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path                = "/"
    interval            = 300
    healthy_threshold   = 2
    unhealthy_threshold = 2
    matcher             = "200,302"
  }
}

# Worker LB Listener
resource "aws_lb_listener" "worker_http" {
  load_balancer_arn = aws_lb.worker.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.worker_http.arn
  }
}
