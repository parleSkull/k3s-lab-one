# Input variables
variable "tags" {
  description = "Tags for worker resources"
  type        = map(any)
  default     = {}
}

variable "vpc_id" {
  description = "ID of VPC to associate with worker resources"
  type        = string
}

variable "egress_cidr_blocks" {
  description = "CIDR blocks for egress traffic"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "instance_type" {
  description = "Type of EC2 instance to use for workers"
  type        = string
}

variable "key_pair_id" {
  description = "Key pair Id for the worker instances"
  type        = string
}

variable "pub_subnets" {
  description = "List of Public subnet Ids (Pri and Sec) to use for resource deployments"
  type        = list(string)
}

variable "priv_subnets" {
  description = "List of Private subnet Ids (Pri and Sec) to use for resource deployments"
  type        = list(string)
}

variable "pub_sn_cidr_blocks" {
  description = "List of Public subnet CIDR blocks (Pri and Sec) to use for resource deployments"
  type        = list(string)
}

variable "priv_sn_cidr_blocks" {
  description = "List of Private subnet CIDR blocks (Pri and Sec) to use for resource deployments"
  type        = list(string)
}
