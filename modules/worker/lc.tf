# Worker instance image data
data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

# Worker Launch configuration
resource "aws_launch_configuration" "worker" {
  name_prefix                 = "worker-"
  image_id                    = data.aws_ami.amazon_linux.id
  instance_type               = var.instance_type
  key_name                    = var.key_pair_id
  security_groups             = [aws_security_group.worker_nodes.id]
  associate_public_ip_address = false

  root_block_device {
    volume_size = 50
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Worker Autoscaling group
resource "aws_autoscaling_group" "worker" {
  name                 = "worker"
  launch_configuration = aws_launch_configuration.worker.name
  min_size             = 1
  max_size             = 2
  desired_capacity     = 1
  vpc_zone_identifier  = [var.priv_subnets[0], var.priv_subnets[1]]
  target_group_arns    = [aws_lb_target_group.worker_http.arn]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    {
    Name = "Worker-node"
  },
    var.tags
  )
}

# TODO: Define function to merger map tags with object tags
/* tag {
    key = "name"
    value = "worker-node"
    propagate_at_launch = true
  } */
  /* tags = merge(
    {
    Name = "Worker-node"
  },
    var.tags
  ) */