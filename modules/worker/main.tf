# Worker module definition #

# Security group for worker nodes
resource "aws_security_group" "worker_nodes" {
  name        = "worker-nodes-allow-tcp"
  description = "Allow traffic to worker nodes"
  vpc_id = var.vpc_id
  tags   = var.tags

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.pub_sn_cidr_blocks[0], var.pub_sn_cidr_blocks[1]]
  }
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.priv_sn_cidr_blocks[0], var.priv_sn_cidr_blocks[1]]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.egress_cidr_blocks
  }
}

# Security group for ALB
resource "aws_security_group" "worker_alb" {
  name        = "worker-alb"
  description = "Allow traffic for ALB"
  vpc_id = var.vpc_id
  tags   = var.tags

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
