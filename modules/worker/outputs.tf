# Outputs
output "elb_dns_name" {
  description = "Public DNS name for the worker elb"
  value       = aws_lb.worker.dns_name
}

output "elb_zone_id" {
  description = "Zone id for the worker elb"
  value       = aws_lb.worker.zone_id
}
