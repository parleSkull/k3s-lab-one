# Route 53
resource "aws_route53_zone" "prik3s_private_zone" {
  name          = "k3s-private-zone"
  force_destroy = true

  vpc {
    vpc_id = var.vpc_id
  }

  tags = var.tags
}

# Record
resource "aws_route53_record" "worker_lb_record" {
  zone_id = aws_route53_zone.prik3s_private_zone.zone_id
  name    = "smks3lab.ryz"
  type    = "A"

  alias {
    name                   = aws_lb.worker.dns_name
    zone_id                = aws_lb.worker.zone_id
    evaluate_target_health = true
  }
}
