# Global outputs
output "worker_elb_dns_name" {
  description = "Public DNS name of the worker load balancer"
  value       = module.worker.elb_dns_name
}

output "bastion_public_ip" {
  description = "Public IP of the bastion instance"
  value       = module.bastion.public_ip
}
